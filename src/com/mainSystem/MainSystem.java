package com.mainSystem;

import com.view.login.LoginForm;

public class MainSystem {

	public MainSystem() {
	}
	
	public void startService() {
		LoginForm loginForm = new LoginForm();
		Thread sub_thread = new Thread(loginForm);
		sub_thread.start();
	}
	
	public static void main(String[] args) {
		new MainSystem().startService();
	}
}
