package com.service;

import java.util.List;

import com.dto.ProblemItemDTO;
import com.factory.DAOFactory;

public class ProblemItem {

	public ProblemItem() {
	}

	public ProblemItemDTO getProblemItem(String c_id) {
		return DAOFactory.getProblemItemDAO().getProblemItem(c_id);
	}

	public List<ProblemItemDTO> getProblemItems(String c_id) {
		return DAOFactory.getProblemItemDAO().getProblemItems(c_id);
	}

	public List<ProblemItemDTO> getProblemItems() {
		return DAOFactory.getProblemItemDAO().getProblemItems();
	}

	public boolean save(ProblemItemDTO problemItemDTO) {
		return DAOFactory.getProblemItemDAO().save(problemItemDTO);
	}

	public boolean delete(String p_id) {
		return DAOFactory.getProblemItemDAO().delete(p_id);
	}

	public boolean update(ProblemItemDTO problemItemDTO) {
		return DAOFactory.getProblemItemDAO().update(problemItemDTO);
	}

	public String getProblemMaxId() {
		return DAOFactory.getProblemItemDAO().getProblemMaxId();
	}

	public List<ProblemItemDTO> getProblemItems(String course_id,
			String pt_id) {
		return DAOFactory.getProblemItemDAO().getProblemItems(course_id,
				pt_id);
	}
}
