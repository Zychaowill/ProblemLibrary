package com.service;

import java.util.List;

import com.dto.TeacherDTO;
import com.factory.DAOFactory;

public class Teacher {
	private String username;
	private String password;

	public Teacher() {
	}

	public Teacher(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public TeacherDTO findTeacherByName() {
		return DAOFactory.getTeacherDAO().findTeacherByName(username);
	}

	public boolean findTeacher() {
		return DAOFactory.getTeacherDAO().findTeacher(username, password);
	}

	public TeacherDTO getTeacher() {
		return DAOFactory.getTeacherDAO().getTeacher(username, password);
	}
	
	public List<TeacherDTO> getTeachers() {
		return DAOFactory.getTeacherDAO().getTeachers();
	}
	
	public TeacherDTO getTeacherById(String teacher_id) {
		return DAOFactory.getTeacherDAO().getTeacherById(teacher_id);
	}
}
