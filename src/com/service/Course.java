package com.service;

import java.util.List;

import com.dto.CourseDTO;
import com.factory.DAOFactory;

public class Course {

	public Course() {
	}

	public CourseDTO getCourseById(String course_id) {
		return DAOFactory.getCourseDAO().getCourseById(course_id);
	}
	
	public List<CourseDTO> getCourses() {
		return DAOFactory.getCourseDAO().getCourses();
	}
}
