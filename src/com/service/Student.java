package com.service;

import java.util.List;

import com.dto.StudentDTO;
import com.factory.DAOFactory;

public class Student {
	private String username;
	private String password;

	public Student() {
	}

	public Student(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public StudentDTO findStudentByName() {
		DAOFactory.getStudentDAO().findStudentByName(username);
		return null;
	}

	public boolean findStudent() {
		return DAOFactory.getStudentDAO().findStudent(username, password);
	}

	public List<StudentDTO> getStudents() {
		return DAOFactory.getStudentDAO().getStudents();
	}

	public boolean saveStudent(StudentDTO studentDTO) {
		return DAOFactory.getStudentDAO().saveStudent(studentDTO);
	}
}
