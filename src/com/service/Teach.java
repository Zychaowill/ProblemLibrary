package com.service;

import java.util.List;

import com.dto.TeachDTO;
import com.factory.DAOFactory;

public class Teach {

	public Teach() {
	}

	public TeachDTO getTeach(String t_id, String c_id) {
		return DAOFactory.getTeachDAO().getTeach(t_id, c_id);
	}

	public TeachDTO getTeachById(String t_id) {
		return DAOFactory.getTeachDAO().getTeachById(t_id);
	}

	public List<TeachDTO> getTeach(String t_id) {
		return DAOFactory.getTeachDAO().getTeach(t_id);
	}

	public List<TeachDTO> getTeaches() {
		return DAOFactory.getTeachDAO().getTeaches();
	}

	public List<String> getCourseId(String t_id) {
		return DAOFactory.getTeachDAO().getCourseId(t_id);
	}

	public String getTeacherId(String c_id) {
		return DAOFactory.getTeachDAO().getTeacherId(c_id);
	}

	public boolean saveTeach(TeachDTO teachDTO) {
		return DAOFactory.getTeachDAO().saveTeach(teachDTO);
	}

	public List<TeachDTO> getTeachesById(String t_id) {
		return DAOFactory.getTeachDAO().getTeachesById(t_id);
	}

	public boolean removeLink(String t_id, String c_id) {
		return DAOFactory.getTeachDAO().removeLink(t_id, c_id);
	}
}
