package com.view.operation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import com.view.subOperation.StudentSelectFrame;
import com.view.subOperation.StudentRegister;

@SuppressWarnings("serial")
public class StudentFrame extends JInternalFrame implements ActionListener {
	private static StudentFrame studentFrame;
	private JPanel contentPane;
	private JLabel L_currentPos;
	private DefaultTableModel tableModel;
	private JDesktopPane desktopPane;

	public StudentFrame() {
		setTitle("通用题库管理系统>>学生模式");
		setSize(new Dimension(1351, 750));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel indexPane = new JPanel();
		indexPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		indexPane.setBounds(0, 0, 197, 720);
		contentPane.add(indexPane);
		indexPane.setLayout(null);

		JButton btnRegister = new JButton("注册账户");
		btnRegister.addActionListener(this);
		btnRegister.setFont(new Font("华文楷体", Font.BOLD, 18));
		btnRegister.setBounds(0, 48, 197, 48);
		indexPane.add(btnRegister);

		JButton btnFind = new JButton("题库状态查询");
		btnFind.addActionListener(this);
		btnFind.setFont(new Font("华文楷体", Font.BOLD, 18));
		btnFind.setBounds(0, 96, 197, 48);
		indexPane.add(btnFind);

		JButton btnHome_Page = new JButton("首页");
		btnHome_Page.addActionListener(this);
		btnHome_Page.setFont(new Font("华文楷体", Font.BOLD, 18));
		btnHome_Page.setBounds(0, 0, 197, 48);
		indexPane.add(btnHome_Page);

		JPanel tipPane = new JPanel();
		tipPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		tipPane.setBounds(197, 0, 1138, 48);
		contentPane.add(tipPane);
		tipPane.setLayout(null);

		JLabel L_currentPosTip = new JLabel("当前位置:");
		L_currentPosTip.setFont(new Font("华文楷体", Font.BOLD, 18));
		L_currentPosTip.setBounds(0, 0, 95, 43);
		tipPane.add(L_currentPosTip);

		L_currentPos = new JLabel("首页");
		L_currentPos.setFont(new Font("华文楷体", Font.BOLD, 18));
		L_currentPos.setBounds(96, 0, 208, 43);
		tipPane.add(L_currentPos);

		JPanel containPane = new JPanel();
		containPane.setBounds(197, 48, 1138, 672);
		contentPane.add(containPane);
		containPane.setLayout(null);

		desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 1138, 672);
		containPane.add(desktopPane);
		try {
			do_home_page_action(null);
		} catch (Exception ex) {

		}

		String[] headers = { "题目内容", "题型名称", "选项内容", "答案", "题表公共信息", "课程名称",
				"备注" };
		tableModel = new DefaultTableModel();
		tableModel.setColumnIdentifiers(headers);

		setVisible(true);
		setResizable(false);
	}

	protected void do_find_action(ActionEvent e) {
		StudentSelectFrame.display();

		try {
			desktopPane.add(StudentSelectFrame.getInstance());
			StudentSelectFrame.getInstance().setClosable(true);
			StudentSelectFrame.getInstance().setMaximizable(true);
			StudentSelectFrame.getInstance().setMaximum(true);
		} catch (Exception ex) {
		}
	}

	protected void do_home_page_action(ActionEvent e) {
		HomePage.display();

		try {
			desktopPane.add(HomePage.getInstance());
			HomePage.getInstance().setClosable(true);
			HomePage.getInstance().setMaximizable(true);
			HomePage.getInstance().setMaximum(true);
		} catch (Exception ex) {
		}
	}

	protected void do_register_action(ActionEvent e) {
		StudentRegister.display();

		try {
			desktopPane.add(StudentRegister.getInstance());
			StudentRegister.getInstance().setClosable(true);
			StudentRegister.getInstance().setMaximizable(true);
			StudentRegister.getInstance().setMaximum(true);
		} catch (Exception ex) {
		}
	}

	public static void display() {
		studentFrame = new StudentFrame();
		studentFrame.setVisible(true);
		studentFrame.setResizable(false);
	}

	public static StudentFrame getInstance() {
		return studentFrame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();

		switch (command) {
		case "首页": {
			L_currentPos.setText("首页");
			do_home_page_action(e);
			break;
		}
		case "注册账户": {
			L_currentPos.setText("注册账户");
			do_register_action(e);
			break;
		}
		case "题库状态查询": {
			L_currentPos.setText("题库状态查询");
			do_find_action(e);
			break;
		}
		default:
			break;
		}
	}
}
