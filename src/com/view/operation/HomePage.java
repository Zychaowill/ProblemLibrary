package com.view.operation;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class HomePage extends JInternalFrame {
	private static HomePage homePage;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePage frame = new HomePage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public HomePage() {
		setSize(1138, 582);
		getContentPane().setLayout(null);

		JLabel L_welcome_tip = new JLabel("欢迎使用通用题库管理系统");
		L_welcome_tip.setHorizontalAlignment(SwingConstants.CENTER);
		L_welcome_tip.setFont(new Font("华文楷体", Font.BOLD, 26));
		L_welcome_tip.setBounds(388, 33, 384, 54);
		getContentPane().add(L_welcome_tip);

		JLabel L_copyright = new JLabel("copyright@yachao");
		L_copyright.setHorizontalAlignment(SwingConstants.CENTER);
		L_copyright.setFont(new Font("华文楷体", Font.BOLD, 26));
		L_copyright.setBounds(427, 478, 267, 54);
		getContentPane().add(L_copyright);
	}

	public static void display() {
		homePage = new HomePage();
		homePage.setVisible(true);
		homePage.setResizable(false);
	}

	public static HomePage getInstance() {
		return homePage;
	}
}
