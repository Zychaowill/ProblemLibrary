package com.view.operation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.view.subOperation.TeacherSelectFrame;

@SuppressWarnings("serial")
public class TeacherFrame extends JInternalFrame implements ActionListener {
	private static TeacherFrame teacherFrame;
	private JPanel contentPane;
	private JLabel L_currentPos;
	private JDesktopPane desktopPane;

	public TeacherFrame() {
		setTitle("通用题库管理系统>>教师模式");
		setSize(new Dimension(1351, 750));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel indexPane = new JPanel();
		indexPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		indexPane.setBounds(0, 0, 197, 720);
		contentPane.add(indexPane);
		indexPane.setLayout(null);

		JButton btnLibManage = new JButton("题库状态管理");
		btnLibManage.setFont(new Font("华文楷体", Font.BOLD, 18));
		btnLibManage.setBounds(0, 48, 197, 48);
		btnLibManage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_libManage_action(e);
			}
		});
		indexPane.add(btnLibManage);

		JButton btnStuManage = new JButton("学生管理");
		btnStuManage.setFont(new Font("华文楷体", Font.BOLD, 18));
		btnStuManage.setBounds(0, 96, 197, 48);
		btnStuManage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_stuManage_action(e);
			}
		});
		indexPane.add(btnStuManage);

		JButton btnHomePage = new JButton("首页");
		btnHomePage.setFont(new Font("华文楷体", Font.BOLD, 18));
		btnHomePage.setBounds(0, 0, 197, 48);
		btnHomePage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_home_page_action(e);
			}
		});
		indexPane.add(btnHomePage);

		JPanel tipPane = new JPanel();
		tipPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		tipPane.setBounds(197, 0, 1138, 48);
		contentPane.add(tipPane);
		tipPane.setLayout(null);

		JLabel L_currentPosTip = new JLabel("当前位置:");
		L_currentPosTip.setFont(new Font("华文楷体", Font.BOLD, 18));
		L_currentPosTip.setBounds(0, 0, 95, 43);
		tipPane.add(L_currentPosTip);

		L_currentPos = new JLabel("首页");
		L_currentPos.setFont(new Font("华文楷体", Font.BOLD, 18));
		L_currentPos.setBounds(96, 0, 208, 43);
		tipPane.add(L_currentPos);

		desktopPane = new JDesktopPane();
		desktopPane.setBounds(197, 48, 1138, 672);
		contentPane.add(desktopPane);
		try {
			do_home_page_action(null);
		} catch (Exception ex) {

		}

		setVisible(true);
		setResizable(false);
	}

	protected void do_libManage_action(ActionEvent e) {
		try {
			L_currentPos.setText("题库状态管理");

			if (HomePage.getInstance() != null) {
				HomePage.getInstance().setClosed(true);
			}

			TeacherSelectFrame.display();

			desktopPane.add(TeacherSelectFrame.getInstance());

			TeacherSelectFrame.getInstance().setMaximizable(true);
			TeacherSelectFrame.getInstance().setClosable(true);
			TeacherSelectFrame.getInstance().setMaximum(true);
		} catch (Exception ex) {
		}
	}

	protected void do_stuManage_action(ActionEvent e) {
		L_currentPos.setText("学生管理");
	}

	protected void do_home_page_action(ActionEvent e) {
		try {
			L_currentPos.setText("首页");

			if (TeacherSelectFrame.getInstance() != null) {
				TeacherSelectFrame.getInstance().setClosed(true);
			}

			HomePage.display();

			desktopPane.add(HomePage.getInstance());

			HomePage.getInstance().setMaximizable(true);
			HomePage.getInstance().setClosable(true);
			HomePage.getInstance().setMaximum(true);
		} catch (Exception ex) {

		}
	}

	public static void display() {
		teacherFrame = new TeacherFrame();
		teacherFrame.setVisible(true);
		teacherFrame.setResizable(false);
	}

	public static TeacherFrame getInstance() {
		return teacherFrame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
