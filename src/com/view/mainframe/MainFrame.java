package com.view.mainframe;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.util.tool.WindowUtil;
import com.view.login.LoginForm;
import com.view.operation.StudentFrame;
import com.view.operation.TeacherFrame;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private static MainFrame mainFrame;
	private JPanel contentPane;
	private JLabel L_welcome;
	private JDesktopPane desktopPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() {
		setTitle("ͨ通用题库管理系统");
		setSize(WindowUtil.getAllScreen());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel titlePane = new JPanel();
		titlePane.setBackground(Color.PINK);
		titlePane.setLayout(null);
		titlePane.setBorder(new LineBorder(new Color(0, 0, 0)));
		titlePane.setBounds(10, 0, 1340, 53);
		contentPane.add(titlePane);

		JLabel L_title_info = new JLabel("通用题库管理系统");
		L_title_info.setHorizontalAlignment(SwingConstants.CENTER);
		L_title_info.setFont(new Font("华文楷体", Font.BOLD, 28));
		L_title_info.setBounds(0, 0, 264, 53);
		titlePane.add(L_title_info);

		JLabel L_welcome_tip = new JLabel("欢迎您：");
		L_welcome_tip.setHorizontalAlignment(SwingConstants.CENTER);
		L_welcome_tip.setFont(new Font("华文楷体", Font.BOLD, 20));
		L_welcome_tip.setBounds(976, 0, 107, 53);
		titlePane.add(L_welcome_tip);

		L_welcome = new JLabel("");
		L_welcome.setHorizontalAlignment(SwingConstants.CENTER);
		L_welcome.setFont(new Font("华文楷体", Font.BOLD, 20));
		L_welcome.setBounds(1081, 0, 132, 53);
		titlePane.add(L_welcome);

		JLabel L_exit = new JLabel("| 退出 |");
		L_exit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				do_exit_action(e);
			}
		});
		L_exit.setHorizontalAlignment(SwingConstants.CENTER);
		L_exit.setFont(new Font("华文楷体", Font.BOLD, 20));
		L_exit.setBounds(1213, 0, 127, 53);
		titlePane.add(L_exit);

		JPanel copyRightPane = new JPanel();
		copyRightPane.setBackground(Color.PINK);
		copyRightPane.setLayout(null);
		copyRightPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		copyRightPane.setBounds(10, 692, 1340, 48);
		contentPane.add(copyRightPane);

		JLabel L_copyright = new JLabel("copyright@yachao");
		L_copyright.setHorizontalAlignment(SwingConstants.CENTER);
		L_copyright.setFont(new Font("华文楷体", Font.BOLD, 22));
		L_copyright.setBounds(556, 10, 254, 28);
		copyRightPane.add(L_copyright);

		desktopPane = new JDesktopPane();
		desktopPane.setBounds(10, 53, 1340, 639);
		contentPane.add(desktopPane);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					do_exit_action(null);
				} catch (Exception ex) {
					// 此处是通过异常捕获来防止抛出本不需要的异常，从而保证程序的正常运行
				}
			}
		});

		setVisible(true);
		setResizable(false);
	}

	public JLabel getL_welcome() {
		return L_welcome;
	}

	public void callTeacherFrame() {
		TeacherFrame.display();

		try {
			desktopPane.add(TeacherFrame.getInstance());
			TeacherFrame.getInstance().setMaximum(true);
		} catch (PropertyVetoException e) {

		}
	}

	public void callStudentFrame() {
		StudentFrame.display();

		try {
			desktopPane.add(StudentFrame.getInstance());
			StudentFrame.getInstance().setMaximum(true);
		} catch (PropertyVetoException e) {

		}
	}

	protected void do_exit_action(MouseEvent e) {
		MainFrame.this.dispose();

		try {
			if (TeacherFrame.getInstance() != null) {
				TeacherFrame.getInstance().setMaximum(false);
				TeacherFrame.getInstance().dispose();
			} else {
				StudentFrame.getInstance().setMaximum(false);
				StudentFrame.getInstance().dispose();
			}
		} catch (Exception ex) {
		}

		LoginForm.display();
	}

	public static void display() {
		mainFrame = new MainFrame();
		mainFrame.setVisible(true);
		mainFrame.setResizable(false);
	}

	public static MainFrame getInstance() {
		return mainFrame;
	}
}
