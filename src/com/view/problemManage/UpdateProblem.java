package com.view.problemManage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.dto.CourseDTO;
import com.dto.ProblemItemDTO;
import com.service.ProblemItem;
import com.util.tool.ProblemType;
import com.util.tool.WindowUtil;

@SuppressWarnings("serial")
public class UpdateProblem extends JFrame {

	private static UpdateProblem updateProblem;
	private JPanel contentPane;
	private JTextField tf_problem_number;
	private JComboBox<String> cbb_type;
	private JTextArea ta_problem_content;
	private JTextArea ta_chooise_content;
	private JTextArea ta_answer;
	private JTextArea ta_remark;
	private JTextArea ta_support;
	private ProblemItemDTO problemItemDTO;
	private CourseDTO courseDTO;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateProblem frame = new UpdateProblem();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public UpdateProblem() {
		setTitle("修改题库状态");
		setBounds(100, 100, 836, 648);
		setLocation(WindowUtil.setLocation(new Dimension(this.getWidth(), this
				.getHeight())));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel L_title = new JLabel("修改题库状态");
		L_title.setFont(new Font("华文楷体", Font.BOLD, 26));
		L_title.setBounds(334, 27, 182, 39);
		contentPane.add(L_title);

		JLabel L_number = new JLabel("问题编号");
		L_number.setHorizontalAlignment(SwingConstants.CENTER);
		L_number.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_number.setBounds(54, 92, 73, 33);
		contentPane.add(L_number);

		tf_problem_number = new JTextField();
		tf_problem_number.setEditable(false);
		tf_problem_number.setFont(new Font("华文楷体", Font.BOLD, 16));
		tf_problem_number.setColumns(10);
		tf_problem_number.setBounds(181, 93, 173, 33);
		contentPane.add(tf_problem_number);

		JLabel L_type = new JLabel("题目类型");
		L_type.setHorizontalAlignment(SwingConstants.CENTER);
		L_type.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_type.setBounds(447, 92, 73, 33);
		contentPane.add(L_type);

		cbb_type = new JComboBox<String>();
		cbb_type.setModel(new DefaultComboBoxModel<String>(ProblemType
				.getNames()));
		cbb_type.setFont(new Font("华文楷体", Font.BOLD, 16));
		cbb_type.setBounds(574, 92, 173, 33);
		cbb_type.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_typeChange_action(e);
			}
		});
		contentPane.add(cbb_type);

		JLabel L_problem_content = new JLabel("题目内容");
		L_problem_content.setHorizontalAlignment(SwingConstants.CENTER);
		L_problem_content.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_problem_content.setBounds(102, 155, 73, 33);
		contentPane.add(L_problem_content);

		JScrollPane sp_problem_content = new JScrollPane();
		sp_problem_content
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		sp_problem_content
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		sp_problem_content.setBounds(54, 198, 173, 167);
		contentPane.add(sp_problem_content);

		ta_problem_content = new JTextArea();
		ta_problem_content.setLineWrap(true);
		ta_problem_content.setFont(new Font("华文楷体", Font.BOLD, 14));
		sp_problem_content.setViewportView(ta_problem_content);

		JLabel L_chooise_content = new JLabel("选项内容");
		L_chooise_content.setHorizontalAlignment(SwingConstants.CENTER);
		L_chooise_content.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_chooise_content.setBounds(359, 155, 73, 33);
		contentPane.add(L_chooise_content);

		JScrollPane sp_chooise_content = new JScrollPane();
		sp_chooise_content
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		sp_chooise_content
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		sp_chooise_content.setBounds(316, 198, 173, 167);
		contentPane.add(sp_chooise_content);

		ta_chooise_content = new JTextArea();
		ta_chooise_content.setLineWrap(true);
		ta_chooise_content.setFont(new Font("华文楷体", Font.BOLD, 14));
		sp_chooise_content.setViewportView(ta_chooise_content);

		JLabel L_answer = new JLabel("答      案");
		L_answer.setHorizontalAlignment(SwingConstants.CENTER);
		L_answer.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_answer.setBounds(626, 155, 73, 33);
		contentPane.add(L_answer);

		JScrollPane sp_answer = new JScrollPane();
		sp_answer
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		sp_answer
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		sp_answer.setBounds(574, 198, 173, 167);
		contentPane.add(sp_answer);

		ta_answer = new JTextArea();
		ta_answer.setLineWrap(true);
		ta_answer.setFont(new Font("华文楷体", Font.BOLD, 14));
		sp_answer.setViewportView(ta_answer);

		JLabel L_remark = new JLabel("备     注");
		L_remark.setHorizontalAlignment(SwingConstants.CENTER);
		L_remark.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_remark.setBounds(54, 397, 73, 33);
		contentPane.add(L_remark);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(181, 390, 566, 57);
		contentPane.add(scrollPane);

		ta_remark = new JTextArea();
		ta_remark.setFont(new Font("华文楷体", Font.BOLD, 14));
		scrollPane.setViewportView(ta_remark);

		JLabel L_support = new JLabel("所属课程");
		L_support.setHorizontalAlignment(SwingConstants.CENTER);
		L_support.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_support.setBounds(54, 482, 73, 33);
		contentPane.add(L_support);

		JScrollPane sp_support = new JScrollPane();
		sp_support
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		sp_support
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		sp_support.setBounds(181, 476, 566, 57);
		contentPane.add(sp_support);

		ta_support = new JTextArea();
		ta_support.setBackground(Color.LIGHT_GRAY);
		ta_support.setEditable(false);
		ta_support.setFont(new Font("华文楷体", Font.BOLD, 14));
		sp_support.setViewportView(ta_support);

		JButton btnUpdate = new JButton("修改");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_update_action(e);
			}
		});
		btnUpdate.setFont(new Font("华文楷体", Font.BOLD, 16));
		btnUpdate.setBounds(111, 561, 173, 39);
		contentPane.add(btnUpdate);

		JButton btnReset = new JButton("复原");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_reset_action(e);
			}
		});
		btnReset.setFont(new Font("华文楷体", Font.BOLD, 16));
		btnReset.setBounds(330, 561, 173, 39);
		contentPane.add(btnReset);

		JButton btnExit = new JButton("取消");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				do_exit_action(e);
			}
		});
		btnExit.setFont(new Font("华文楷体", Font.BOLD, 16));
		btnExit.setBounds(545, 561, 173, 39);
		contentPane.add(btnExit);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					do_exit_action(null);
				} catch (Exception ex) {
					// 此处不再进行相应处理
				}
			}
		});
	}

	public JTextArea getTa_chooise_content() {
		return ta_chooise_content;
	}

	protected void do_typeChange_action(ActionEvent e) {
		String type = (String) cbb_type.getSelectedItem();

		switch (type) {
		case "单选题":
		case "多选题":
			ta_chooise_content.setEnabled(true);
			ta_chooise_content.setBackground(Color.WHITE);
			break;
		case "判断题":
		case "问答题":
			ta_chooise_content.setEnabled(false);
			ta_chooise_content.setBackground(Color.LIGHT_GRAY);
			break;
		default:
			break;
		}
	}

	protected void do_update_action(ActionEvent e) {
		Object[] params = new Object[7];
		params[0] = tf_problem_number.getText().trim();
		params[1] = cbb_type.getSelectedItem();
		params[2] = ta_problem_content.getText().trim();
		params[3] = ta_chooise_content.getText().trim();
		params[4] = ta_answer.getText().trim();
		params[5] = ta_remark.getText().trim();
		params[6] = courseDTO.getC_id();

		boolean[] result = validateProblem(params);

		if (result[0]) {
			if (result[1]) {
				if (result[2]) {
					ProblemItemDTO problemItem = new ProblemItemDTO(params);

					boolean success = new ProblemItem().update(problemItem);

					if (success) {
						JOptionPane.showMessageDialog(null, "修改成功！");
						do_exit_action(e);
					} else {
						JOptionPane.showMessageDialog(null, "修改失败!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "该题为单选,答案有误!");
				}
			} else {
				JOptionPane.showMessageDialog(null, "存在SQL注入现象！");
			}
		} else {
			JOptionPane.showMessageDialog(null, "请完善信息！");
		}
	}

	private boolean[] validateProblem(Object... params) {
		boolean[] result = { true, true, true };

		for (int i = 0; i < params.length; i++) {
			if (i == 0 || i == 3 || i == 5) {
				continue;
			}
			if ("".equals(params[i])) {
				result[0] = false;
			}
			if ("1".equals(params[i])) {
				result[1] = false;
			}
		}

		if ("单选题".equals(params[1])) {
			String answer = (String) params[4];
			if (answer.length() > 1) {
				result[2] = false;
			}
		}
		return result;
	}

	public void setDTO(ProblemItemDTO problemItemDTO, CourseDTO courseDTO) {
		this.problemItemDTO = problemItemDTO;
		this.courseDTO = courseDTO;
		/* 初始化数据 */
		initNewData();
	}

	private void initNewData() {
		tf_problem_number.setText(problemItemDTO.getP_id());
		cbb_type.setSelectedIndex((Integer.parseInt(problemItemDTO.getPt_id()) - 1));
		ta_problem_content.setText(problemItemDTO.getP_content());
		ta_chooise_content.setText(problemItemDTO.getChooise_content());
		ta_answer.setText(problemItemDTO.getAnswer());
		ta_remark.setText(problemItemDTO.getRemark());
		ta_support.setText(courseDTO.getC_name());
	}

	protected void do_reset_action(ActionEvent e) {
		initNewData();
	}

	protected void do_exit_action(ActionEvent e) {
		UpdateProblem.this.dispose();
	}

	public static void display() {
		updateProblem = new UpdateProblem();
		updateProblem.setVisible(true);
		updateProblem.setResizable(false);
	}

	public static UpdateProblem getInstance() {
		return updateProblem;
	}
}
