package com.view.subOperation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.dto.StudentDTO;
import com.service.Student;
import com.util.tool.WindowUtil;

@SuppressWarnings("serial")
public class StudentRegister extends JInternalFrame implements ActionListener {

	private static StudentRegister register;
	private final JPanel contentPane = new JPanel();
	private JTextField tf_username;
	private JTextField tf_password;
	private JRadioButton rb_sex_male;
	private JRadioButton rb_sex_female;
	private String sex = "男";
	private JTextField tf_age;
	private JTextField tf_tel;
	private JComboBox<String> cbb_depart;
	private JComboBox<String> cbb_major;
	private DefaultComboBoxModel<String> departModel, majorModel;
	private JTextField tf_email;

	public StudentRegister() {
		setBounds(100, 100, 663, 385);
		setLocation(WindowUtil.setLocation(new Dimension(this.getWidth(), this
				.getHeight())));
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);

		JLabel L_username = new JLabel("姓       名");
		L_username.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_username.setBounds(42, 93, 74, 26);
		contentPane.add(L_username);

		tf_username = new JTextField();
		tf_username.setFont(new Font("华文楷体", Font.BOLD, 16));
		tf_username.setBounds(153, 94, 155, 26);
		contentPane.add(tf_username);
		tf_username.setColumns(10);

		JLabel L_password = new JLabel("密       码");
		L_password.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_password.setBounds(338, 93, 74, 26);
		contentPane.add(L_password);

		tf_password = new JTextField();
		tf_password.setFont(new Font("华文楷体", Font.BOLD, 14));
		tf_password.setColumns(10);
		tf_password.setBounds(445, 93, 159, 26);
		contentPane.add(tf_password);

		JLabel L_age = new JLabel("年       龄");
		L_age.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_age.setBounds(338, 136, 74, 26);
		contentPane.add(L_age);

		tf_age = new JTextField();
		tf_age.setFont(new Font("华文楷体", Font.BOLD, 16));
		tf_age.setColumns(10);
		tf_age.setBounds(445, 140, 159, 26);
		contentPane.add(tf_age);

		JLabel L_sex = new JLabel("性       别");
		L_sex.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_sex.setBounds(42, 140, 74, 26);
		contentPane.add(L_sex);

		ButtonGroup btnGroup = new ButtonGroup();
		rb_sex_male = new JRadioButton("男");
		rb_sex_male.setBackground(Color.PINK);
		rb_sex_male.setSelected(true);
		rb_sex_male.addItemListener(sexListener);
		rb_sex_male.setBounds(175, 143, 44, 23);
		contentPane.add(rb_sex_male);
		btnGroup.add(rb_sex_male);

		rb_sex_female = new JRadioButton("女");
		rb_sex_female.setBackground(Color.PINK);
		rb_sex_female.setBounds(233, 143, 44, 23);
		rb_sex_female.addItemListener(sexListener);
		contentPane.add(rb_sex_female);
		btnGroup.add(rb_sex_female);

		JLabel L_tel = new JLabel("联系电话");
		L_tel.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_tel.setBounds(42, 198, 74, 26);
		contentPane.add(L_tel);

		tf_tel = new JTextField();
		tf_tel.setFont(new Font("华文楷体", Font.BOLD, 14));
		tf_tel.setColumns(10);
		tf_tel.setBounds(153, 199, 155, 26);
		contentPane.add(tf_tel);

		JLabel L_depart = new JLabel("学       院");
		L_depart.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_depart.setBounds(42, 251, 74, 26);
		contentPane.add(L_depart);

		cbb_depart = new JComboBox<String>();
		departModel = new DefaultComboBoxModel<String>();
		cbb_depart.setModel(departModel);
		cbb_depart.setFont(new Font("华文楷体", Font.BOLD, 16));
		cbb_depart.setBounds(153, 252, 155, 26);
		contentPane.add(cbb_depart);

		JLabel L_email = new JLabel("邮       箱");
		L_email.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_email.setBounds(338, 198, 74, 26);
		contentPane.add(L_email);

		tf_email = new JTextField();
		tf_email.setFont(new Font("华文楷体", Font.BOLD, 14));
		tf_email.setColumns(10);
		tf_email.setBounds(445, 198, 159, 26);
		contentPane.add(tf_email);

		getContentPane().add(contentPane, BorderLayout.CENTER);

		JLabel label = new JLabel("专       业");
		label.setFont(new Font("华文楷体", Font.BOLD, 16));
		label.setBounds(338, 250, 81, 26);
		contentPane.add(label);

		cbb_major = new JComboBox<String>();
		majorModel = new DefaultComboBoxModel<String>();
		cbb_major.setModel(majorModel);
		cbb_major.setFont(new Font("华文楷体", Font.BOLD, 16));
		cbb_major.setBounds(445, 251, 159, 26);
		contentPane.add(cbb_major);

		loadInfo();

		JLabel L_s_id = new JLabel("学       号");
		L_s_id.setFont(new Font("华文楷体", Font.BOLD, 16));
		L_s_id.setBounds(212, 29, 74, 26);
		contentPane.add(L_s_id);

		tf_s_id = new JTextField();
		tf_s_id.setFont(new Font("华文楷体", Font.BOLD, 14));
		tf_s_id.setColumns(10);
		tf_s_id.setBounds(323, 29, 159, 26);
		contentPane.add(tf_s_id);
		{
			JButton btnRegister = new JButton("注册");
			btnRegister.setFont(new Font("华文楷体", Font.BOLD, 14));
			btnRegister.setBounds(211, 309, 97, 27);
			contentPane.add(btnRegister);
			btnRegister.setActionCommand("注册");
			btnRegister.addActionListener(this);
			getRootPane().setDefaultButton(btnRegister);
		}
		{
			JButton cancelButton = new JButton("取消");
			cancelButton.setFont(new Font("华文楷体", Font.BOLD, 14));
			cancelButton.setBounds(349, 309, 97, 27);
			contentPane.add(cancelButton);
			cancelButton.setActionCommand("取消");
			cancelButton.addActionListener(this);
		}

		setVisible(true);
		setResizable(false);
	}

	private void loadInfo() {
		String[] departs = { "", "机械工程学院", "材料科学与工程学院", "应用科学学院", "电子信息工程学院",
				"经济与管理学院", "计算机科学与技术学院", "法学院", "人文社会科学院", "外国语学院", "艺术学院",
				"体育学院", "成人教育学院", "交通与物流学院", "华科学院", "化学与生物工程学院" };
		String[] majors = { "", "机械电子工程", "车辆工程", "测控技术与仪器", "机械设计制造及自动化",
				"材料成型及控制工程", "冶金工程", "工业工程", "电子商务", "经济学", "会计学", "网络工程",
				"软件工程", "计算机科学与技术", "自动化", "电子信息工程", "通信工程" };

		for (int i = 0; i < departs.length; i++) {
			departModel.addElement(departs[i]);
		}

		for (int j = 0; j < majors.length; j++) {
			majorModel.addElement(majors[j]);
		}
	}

	ItemListener sexListener = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			if (rb_sex_female.isSelected()) {
				sex = "女";
			}
			if (rb_sex_male.isSelected()) {
				sex = "男";
			}
		}
	};
	private JTextField tf_s_id;

	protected void do_exit_action(WindowEvent e) {
		StudentRegister.this.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();

		switch (command) {
		case "注册": {
			Object[] params = new Object[9];
			params[0] = tf_s_id.getText().trim();
			params[1] = tf_username.getText().trim();
			params[2] = tf_password.getText().trim();
			params[3] = sex;
			params[4] = tf_age.getText().trim();
			params[5] = tf_tel.getText().trim();
			params[6] = tf_email.getText().trim();
			params[7] = cbb_depart.getSelectedItem();
			params[8] = cbb_major.getSelectedItem();

			boolean[] result = validateStudent(params);
			if (result[0]) {
				if (result[1]) {
					StudentDTO studentDTO = new StudentDTO(params);
					boolean success = new Student().saveStudent(studentDTO);

					if (success) {
						JOptionPane.showMessageDialog(null, "注册成功！");
						clear();
					} else {
						JOptionPane.showMessageDialog(null, "注册失败！");
					}
				} else {
					JOptionPane.showMessageDialog(null, "信息格式有误！");
				}
			} else {
				JOptionPane.showMessageDialog(null, "用户名/密码不能为空！");
			}

			break;
		}
		case "取消": {
			try {
				do_exit_action(null);
			} catch (Exception ex) {
				// 此处不作相应处理
			}
			break;
		}
		default:
			break;
		}
	}

	private void clear() {
		tf_s_id.setText("");
		tf_username.setText("");
		tf_password.setText("");
		rb_sex_male.setSelected(true);
		tf_age.setText("");
		tf_tel.setText("");
		tf_email.setText("");
		cbb_depart.setSelectedIndex(0);
		cbb_major.setSelectedIndex(0);

	}

	private boolean[] validateStudent(Object[] params) {
		boolean[] result = { true, true };

		if ("".equals(params[0]) || "".equals(params[1])) {
			result[0] = false;
		}

		for (int i = 0; i < params.length; i++) {
			if ("1".equals("")) {
				result[1] = false;
			}
		}

		return result;
	}

	public static void display() {
		register = new StudentRegister();
		register.setVisible(true);
		register.setResizable(false);
	}

	public static StudentRegister getInstance() {
		return register;
	}
}
