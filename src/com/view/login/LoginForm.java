package com.view.login;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import com.dto.StudentDTO;
import com.dto.TeacherDTO;
import com.service.Student;
import com.service.Teacher;
import com.util.tool.WindowUtil;
import com.view.mainframe.MainFrame;
import com.view.subOperation.TeacherSelectFrame;

@SuppressWarnings("serial")
public class LoginForm extends JFrame implements Runnable {

	private static LoginForm loginForm;
	private JPanel contentPane;
	private JLabel L_username;
	private JLabel L_password;
	private JComboBox<String> cbb_username;
	private DefaultComboBoxModel<String> usernameModel;
	private JPasswordField pf_password;
	private JRadioButton rb_teacher_model;
	private JRadioButton rb_student_model;
	private JButton btnLogin;
	private String login_model = "教师";

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginForm frame = new LoginForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public LoginForm() {
		setTitle("ͨ通用题库管理系统>>登录");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(WindowUtil.getAllScreen());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel L_login_background = new JLabel("");
		L_login_background.setIcon(new ImageIcon("./images/sea.jpg"));
		L_login_background.setBounds(0, 0, 1366, 768);
		contentPane.add(L_login_background);

		JPanel loginPane = new JPanel();
		loginPane.setBounds(890, 5, 455, 720);
		L_login_background.add(loginPane);
		loginPane.setLayout(null);
		loginPane.setOpaque(false);

		cbb_username = new JComboBox<String>();
		usernameModel = new DefaultComboBoxModel<String>();
		load_teacher_model(); // 装载教师模式数据

		cbb_username.setModel(usernameModel);
		cbb_username.setFont(new Font("华文楷体", Font.BOLD, 16));
		cbb_username.setEditable(true);
		cbb_username.setBounds(187, 269, 212, 33);

		cbb_username.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pf_password.setText("");
			}
		});
		loginPane.add(cbb_username);

		pf_password = new JPasswordField();
		pf_password.setFont(new Font("华文楷体", Font.BOLD, 12));
		pf_password.setEchoChar('●');
		pf_password.setBounds(187, 312, 212, 33);
		loginPane.add(pf_password);

		JPanel modelPane = new JPanel();
		modelPane.setLayout(null);
		modelPane.setOpaque(false);
		modelPane.setBackground(Color.WHITE);
		modelPane.setBounds(187, 352, 212, 39);
		loginPane.add(modelPane);

		ButtonGroup btnGroup = new ButtonGroup();
		rb_teacher_model = new JRadioButton("教师");
		rb_teacher_model.setBounds(18, 6, 57, 31);
		rb_teacher_model.setSelected(true);
		rb_teacher_model.setOpaque(false);
		rb_teacher_model.addItemListener(login_model_listener);
		modelPane.add(rb_teacher_model);
		btnGroup.add(rb_teacher_model);

		rb_student_model = new JRadioButton("学生");
		rb_student_model.setBounds(143, 6, 57, 31);
		rb_student_model.setOpaque(false);
		rb_student_model.addItemListener(login_model_listener);
		modelPane.add(rb_student_model);
		btnGroup.add(rb_student_model);

		btnLogin = new JButton("登录");
		btnLogin.setFont(new Font("华文楷体", Font.BOLD, 16));
		btnLogin.setBounds(187, 399, 100, 39);
		btnLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_login_action(e);
			}
		});
		loginPane.add(btnLogin);

		L_username = new JLabel("用户名 :");
		L_username.setFont(new Font("华文楷体", Font.BOLD, 18));
		L_username.setBounds(84, 270, 79, 33);
		loginPane.add(L_username);

		L_password = new JLabel("密   码 :");
		L_password.setFont(new Font("华文楷体", Font.BOLD, 18));
		L_password.setBounds(84, 312, 79, 33);
		loginPane.add(L_password);

		JLabel L_tip = new JLabel("用户登录 / Login");
		L_tip.setFont(new Font("华文楷体", Font.BOLD, 22));
		L_tip.setBounds(84, 199, 315, 45);
		loginPane.add(L_tip);

		JButton btnReset = new JButton("重置");
		btnReset.setFont(new Font("华文楷体", Font.BOLD, 16));
		btnReset.setBounds(299, 399, 100, 39);
		btnReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_reset_action(e);

			}
		});
		loginPane.add(btnReset);

//		setVisible(true);
//		setResizable(false);
	}

	ItemListener login_model_listener = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			if (rb_teacher_model.isSelected()) {
				login_model = "教师";
				load_teacher_model();
			} else if (rb_student_model.isSelected()) {
				login_model = "学生";
				load_student_model();
			}
		}
	};

	protected void do_login_action(ActionEvent e) {
		Object[] params = new Object[2];
		params[0] = cbb_username.getSelectedItem();
		params[1] = new String(pf_password.getPassword());

		if (validateInfo(params)) {
			if ("教师".equals(login_model)) {
				boolean success = false;
				Teacher t_service = new Teacher((String) params[0],
						(String) params[1]);
				success = t_service.findTeacher();

				if (success) {
					LoginForm.this.dispose();
					/* 教师模式登录后的界面 */
					MainFrame.display();
					MainFrame.getInstance().callTeacherFrame();
					MainFrame.getInstance().getL_welcome()
							.setText(params[0] + "");
					/* 模拟session会话模式进行数据传送 */
					TeacherDTO teacherDTO = t_service.getTeacher();
					TeacherSelectFrame.getInstance().setTeacherDTO(teacherDTO);
				}
			} else if ("学生".equals(login_model)) {
				boolean success = false;
				Student s_service = new Student((String) params[0],
						(String) params[1]);
				success = s_service.findStudent();

				if (success) {
					LoginForm.this.dispose();
					/* 学生模式登录后的界面 */
					MainFrame.display();
					MainFrame.getInstance().callStudentFrame();
					MainFrame.getInstance().getL_welcome()
							.setText(params[0] + "");
				}
			}

		} else {
			JOptionPane.showMessageDialog(null, "用户名/密码不能为空！");
		}

	}

	/* 装载学生信息 */
	protected void load_student_model() {
		if (usernameModel.getSize() > 0) {
			usernameModel.removeAllElements();
		}

		List<StudentDTO> students = null;
		students = new Student().getStudents();

		int student_count = students.size();
		for (int i = 0; i < student_count; i++) {
			StudentDTO student = students.get(i);
			usernameModel.addElement(student.getName());
		}
	}

	/* 装载教师信息 */
	protected void load_teacher_model() {
		if (usernameModel.getSize() > 0) {
			usernameModel.removeAllElements();
		}

		List<TeacherDTO> teachers = null;
		teachers = new Teacher().getTeachers();

		int teacher_count = teachers.size();
		for (int i = 0; i < teacher_count; i++) {
			TeacherDTO teacher = teachers.get(i);
			usernameModel.addElement(teacher.getName());
		}
	}

	protected void do_reset_action(ActionEvent e) {
		cbb_username.setSelectedIndex(0);
		pf_password.setText("");
	}

	private boolean validateInfo(Object... params) {
		boolean result = true;

		for (int i = 0; i < params.length; i++) {
			if ("".equals(params[i])) {
				result = false;
			}
		}

		return result;
	}

	public static void display() {
		loginForm = new LoginForm();
		loginForm.setVisible(true);
		loginForm.setResizable(false);
	}

	public static LoginForm getInstance() {
		return loginForm;
	}

	@Override
	public void run() {
		display();
	}
}