package com.dto;

public class StudentDTO {
	private String s_id;
	private String user_name;
	private String name;
	private String password;
	private String sex;
	private int age;
	private String telephone;
	private String email;
	private String depart;
	private String major;

	public StudentDTO() {

	}

	public StudentDTO(Object... params) {
		try {
			this.s_id = (String) params[0];
			this.user_name = (String) params[1];
			this.name = (String) params[1];
			this.password = (String) params[2];
			this.sex = (String) params[3];
			this.age = Integer.parseInt(params[4] + "");
			this.telephone = (String) params[5];
			this.email = (String) params[6];
			this.depart = (String) params[7];
			this.major = (String) params[8];
		} catch (Exception ex) {
		}
	}

	public String getS_id() {
		return s_id;
	}

	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDepart() {
		return depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	@Override
	public String toString() {
		return "StudentDTO [s_id=" + s_id + ", user_name=" + user_name
				+ ", name=" + name + ", password=" + password + ", sex=" + sex
				+ ", age=" + age + ", telephone=" + telephone + ", email="
				+ email + ", depart=" + depart + ", major=" + major + "]";
	}

}
