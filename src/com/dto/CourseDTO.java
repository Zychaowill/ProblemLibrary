package com.dto;

public class CourseDTO {
	private String c_id;
	private String c_name;
	private String support;

	public CourseDTO() {
	}

	public CourseDTO(String c_id, String c_name, String support) {
		super();
		this.c_id = c_id;
		this.c_name = c_name;
		this.support = support;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getC_name() {
		return c_name;
	}

	public void setC_name(String c_name) {
		this.c_name = c_name;
	}

	public String getSupport() {
		return support;
	}

	public void setSupport(String support) {
		this.support = support;
	}

	@Override
	public String toString() {
		return c_name;
	}
}
