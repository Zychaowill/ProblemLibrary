package com.dto;

public class TeachDTO {
	private String t_id;
	private String c_id;
	private String term = "2015-01";
	private String remark;

	public TeachDTO() {
	}

	public TeachDTO(String t_id, String c_id) {
		super();
		this.t_id = t_id;
		this.c_id = c_id;
	}

	public String getT_id() {
		return t_id;
	}

	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "TeachDTO [t_id=" + t_id + ", c_id=" + c_id + ", term=" + term
				+ ", remark=" + remark + "]";
	}

}
