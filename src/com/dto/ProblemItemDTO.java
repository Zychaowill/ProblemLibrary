package com.dto;

import com.util.tool.CommonUtil;
import com.util.tool.ProblemType;

public class ProblemItemDTO {
	private String p_id;
	private String p_content;
	private String pt_id;
	private String chooise_content;
	private String answer;
	private String p_common_info = "来自题库";
	private String c_id;
	private String remark;

	public ProblemItemDTO() {
	}

	public ProblemItemDTO(Object... params) {
		try {
			this.p_id = (String) params[0];
			this.pt_id = ProblemType.getIndex((String) params[1]);
			this.p_content = (String) params[2];
			this.chooise_content = (String) params[3];
			this.answer = (String) params[4];
			if (params[5] != null) {
				this.remark = (String) params[5];
			}
			this.c_id = (String) params[6];
		} catch (Exception ex) {
			// 此处不再进行相应处理
		}
	}

	public String getP_id() {
		return p_id;
	}

	public void setP_id(String p_id) {
		this.p_id = p_id;
	}

	public String getP_content() {
		return p_content;
	}

	public void setP_content(String p_content) {
		this.p_content = p_content;
	}

	public String getPt_id() {
		return pt_id;
	}

	public void setPt_id(String pt_id) {
		this.pt_id = pt_id;
	}

	public String getChooise_content() {
		return chooise_content;
	}

	public void setChooise_content(String chooise_content) {
		this.chooise_content = chooise_content;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getP_common_info() {
		return p_common_info;
	}

	public void setP_common_info(String p_common_info) {
		this.p_common_info = p_common_info;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "ProblemItemDTO [p_id=" + p_id + ", p_content=" + p_content
				+ ", pt_id=" + pt_id + ", chooise_content=" + chooise_content
				+ ", answer=" + answer + ", p_common_info=" + p_common_info
				+ ", c_id=" + c_id + ", remark=" + remark + "]";
	}
}
