package com.dto;

public class KonwledgePoint {
	private String kp_id;
	private String kp_content;
	private String chapter_id;

	public KonwledgePoint() {
	}

	public KonwledgePoint(String kp_id, String kp_content, String chapter_id) {
		super();
		this.kp_id = kp_id;
		this.kp_content = kp_content;
		this.chapter_id = chapter_id;
	}

	public String getKp_id() {
		return kp_id;
	}

	public void setKp_id(String kp_id) {
		this.kp_id = kp_id;
	}

	public String getKp_content() {
		return kp_content;
	}

	public void setKp_content(String kp_content) {
		this.kp_content = kp_content;
	}

	public String getChapter_id() {
		return chapter_id;
	}

	public void setChapter_id(String chapter_id) {
		this.chapter_id = chapter_id;
	}

	@Override
	public String toString() {
		return "KonwledgePoint [kp_id=" + kp_id + ", kp_content=" + kp_content
				+ ", chapter_id=" + chapter_id + "]";
	}

}
