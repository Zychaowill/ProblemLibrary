package com.dto;

public class ChapterDTO {
	private String chapter_id;
	private String chapter_name;
	private String c_id;

	public ChapterDTO() {
	}

	public ChapterDTO(String chapter_id, String chapter_name, String c_id) {
		super();
		this.chapter_id = chapter_id;
		this.chapter_name = chapter_name;
		this.c_id = c_id;
	}

	public String getChapter_id() {
		return chapter_id;
	}

	public void setChapter_id(String chapter_id) {
		this.chapter_id = chapter_id;
	}

	public String getChapter_name() {
		return chapter_name;
	}

	public void setChapter_name(String chapter_name) {
		this.chapter_name = chapter_name;
	}

	public String getC_id() {
		return c_id;
	}

	public void setC_id(String c_id) {
		this.c_id = c_id;
	}

	@Override
	public String toString() {
		return "ChapterDTO [chapter_id=" + chapter_id + ", chapter_name="
				+ chapter_name + ", c_id=" + c_id + "]";
	}

}
