package com.factory;

import com.dao.CourseDAO;
import com.dao.ProblemItemDAO;
import com.dao.StudentDAO;
import com.dao.TeachDAO;
import com.dao.TeacherDAO;
import com.dao.impl.CourseDAOImpl;
import com.dao.impl.ProblemItemDAOImpl;
import com.dao.impl.StudentDAOImpl;
import com.dao.impl.TeachDAOImpl;
import com.dao.impl.TeacherDAOImpl;

public class DAOFactory {

	public DAOFactory() {
	}
	
	public static StudentDAO getStudentDAO() {
		return new StudentDAOImpl();
	}
	
	public static TeacherDAO getTeacherDAO() {
		return new TeacherDAOImpl();
	}
	
	public static CourseDAO getCourseDAO() {
		return new CourseDAOImpl();
	}
	
	public static TeachDAO getTeachDAO() {
		return new TeachDAOImpl();
	}
	
	public static ProblemItemDAO getProblemItemDAO() {
		return new ProblemItemDAOImpl();
	}
}
