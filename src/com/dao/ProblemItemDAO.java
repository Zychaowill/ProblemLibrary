package com.dao;

import java.util.List;

import com.dto.ProblemItemDTO;

public interface ProblemItemDAO {

	public ProblemItemDTO getProblemItem(String c_id);

	public List<ProblemItemDTO> getProblemItems(String c_id);

	public List<ProblemItemDTO> getProblemItems();

	public boolean save(ProblemItemDTO problemItemDTO);

	public boolean delete(String p_id);

	public boolean update(ProblemItemDTO problemItemDTO);

	public String getProblemMaxId();

	public List<ProblemItemDTO> getProblemItems(String course_id,
			String pt_id);
}
