package com.dao;

import java.util.List;

import com.dto.CourseDTO;

public interface CourseDAO {
	public CourseDTO getCourseById(String course_id);

	public List<CourseDTO> getCourses();
	
}
