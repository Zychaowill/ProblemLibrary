package com.dao;

import java.util.List;

import com.dto.TeachDTO;

public interface TeachDAO {
	public TeachDTO getTeach(String t_id, String c_id);

	public TeachDTO getTeachById(String t_id);

	public List<TeachDTO> getTeach(String t_id);

	public List<String> getCourseId(String t_id);

	public String getTeacherId(String c_id);

	public List<TeachDTO> getTeaches();

	public boolean saveTeach(TeachDTO teachDTO);

	public List<TeachDTO> getTeachesById(String t_id);

	public boolean removeLink(String t_id, String c_id);
}
