package com.dao;

import java.util.List;

import com.dto.StudentDTO;

public interface StudentDAO {
	public StudentDTO findStudentByName(String username);
	
	public boolean findStudent(String username, String password);
	
	public List<StudentDTO> getStudents();
	
	public boolean saveStudent(StudentDTO studentDTO);
}
