package com.dao;

import java.util.List;

import com.dto.TeacherDTO;

public interface TeacherDAO {
	public TeacherDTO findTeacherByName(String username);

	public boolean findTeacher(String username, String password);

	public TeacherDTO getTeacher(String username, String password);

	public List<TeacherDTO> getTeachers();
	
	public TeacherDTO getTeacherById(String teacher_id);
}
