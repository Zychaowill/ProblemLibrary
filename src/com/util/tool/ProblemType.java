package com.util.tool;

import java.util.Vector;

/**
 * 题型类型
 * 
 * @author yachao
 *
 */
public enum ProblemType {
	SINGLE("1", "单选题"), MULTIPLE("2", "多选题"), JUDGE("3", "判断题"), QUESTION("4",
			"问答题");

	private String index;
	private String name;

	private ProblemType(String index, String name) {
		this.index = index;
		this.name = name;
	}

	public static Vector<String> getNames() {
		Vector<String> names = new Vector<String>();
		for (ProblemType t : ProblemType.values()) {
			names.add(t.name);
		}
		return names;
	}

	public static String getIndex(String name) {
		for (ProblemType t : ProblemType.values()) {
			if (t.name.equals(name)) {
				return t.index;
			}
		}
		return null;
	}

	public static String getName(String index) {
		for (ProblemType t : ProblemType.values()) {
			if (t.index.equals(index)) {
				return t.name;
			}
		}
		return null;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
