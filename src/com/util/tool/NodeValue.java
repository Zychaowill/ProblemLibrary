package com.util.tool;

/**
 * 对象隐藏绑定
 * 
 * @author yachao
 *
 */
public class NodeValue {

	private String name;

	private Object object;

	public NodeValue() {

	}

	public NodeValue(String name, Object object) {
		super();
		this.name = name;
		this.object = object;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	@Override
	public String toString() {
		return name;
	}
}
